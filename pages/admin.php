

<?php 
session_start();

?>
 

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
		<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/style2.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
		<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
		<!-- <link rel="stylesheet" href="../bootstrap_4/css/bootstrap.min.css"> -->
	</head>

	<body class="corp ">
		<div class="container-fluid" style="">
		    <div class="row">
			    <div class=" col-md-12">
				   <nav class="navbar navbar-expand ">
					   <div class="col-md-offset-8 col-md-4 col-sm-offset-8 col-sm-4">
						    <ul class="nav navbar-nav pull-right " style="padding-right: 50px;">
								<li class="navbar-btn mr  nav1"><span class="nom"> <?php echo $_SESSION['ADMIN']['nom']." ".$_SESSION['ADMIN']['prenom'] ?></li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<?php echo "<img class='profil' src='../images/".$_SESSION['ADMIN']['photo']."'>" ?>
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="Deconnexionadmin.php">Deconnexion</a>
										<div class="dropdown-divider"></div>
									</div>
								</li>
							</ul>
						</div>
					</nav>
				</div> 
			</div>
			<div class="container">
			<table class="table table-bordered">
				<thead class="thead-light">
					<tr style="background-color: black;">
						<th class="text-center id"style="width: 30px;"> id </th>
						<th class="text-center id"style="width: 30px;"> photo </th>
						<th class="text-center nom"style="width: 250px;"> Nom </th>
						<th class="text-center prenom" style="width: 250px;"> Prenom </th>
						<th class="text-center etat" style="width: 80px;"> Etat </th>
						<th class="text-center action" style="width: 150px;"> Action </th>
					</tr>
				</thead>
				<tbody style="background-color: rgba(0, 0, 0, 0.8);">
					<?php
					$bdd = new PDO('mysql:host=localhost;dbname=users', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
					$response = $bdd->query('SELECT * FROM utilisateur WHERE niveau!=5');
					$i = 0;
					$user = array();
					while ($donnees = $response->fetch()) {
						$user[$i] = $donnees; ?>
						<tr>
							<th ><?php echo $i; ?></th>
							<td><img src="../images/<?php echo $donnees['photo'];?>" style=" width: 80px; height: 80px; border-radius: 50%;"></td>
							<td style="text-align:center; vertical-align: middle;"><?php echo $donnees['nom']; ?></td> 
							<td style="text-align:center; vertical-align: middle;"><?php echo $donnees['prenom']; ?></td> 
							<td style="text-align:center; vertical-align: middle;">
								<?php if ($donnees['niveau'] == 1) { ?>
									<span style='color:green;font-weight:800;'> actif <span>
									<?php } else if ($donnees['niveau'] == 2) {  ?>
										<span style='color:orange;font-weight:800;'> inactif <span>
										<?php } else { ?>
											<span style='color:red;font-weight:800;'> supprimer <span>
											<?php  } ?>
										</td>
										<td  style="text-align:center; vertical-align: middle;"> 
											<div style="font-size: 1px;"> 
												<form enctype="multipart/form-data" method="post" action="actionsadmin.php" style=" display: inline-block;">
													<a href="#">
														<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
														<input type="hidden" name="N2"  value="editer">
														<button  type="submit" style="background-color:inherit; border: none;" href="#    visualiser" data-toggle="modal" data-target="">
															<span class="fa fa-eye editer" style="color:blue; font-size: 16px;">  </span>
														</button>
													</a>
												</form>
												<?php if($donnees['niveau'] <=2 ){ ?>
													<form enctype="multipart/form-data" method="post" action="actionsadmin.php" style=" display: inline-block;">
														<a href="#">
															<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
															<input type="hidden" name="N2"  value="supprimer">
															<button  type="submit" style="background-color:inherit; border: none;">
																<span class="fa fa-trash Supprimer" style="margin-left: 13px;color:red; font-size: 12px;">  </span>
															</button>
														</a>
													</form>
												<?php 	} 
												if ($donnees['niveau'] == 1) { ?>
													<form enctype="multipart/form-data" method="post" action="actionsadmin.php" style=" display: inline-block;">
														<a href="#">
															<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
															<input type="hidden" name="N2"  value="desactiver">
															<button  type="submit" style="background-color:inherit; border: none;">
																<span class="glyphicon glyphicon-remove-circle activer_inverse" style="margin-left: 10px;color:orange; font-size: 15px;">  </span>
															</button>
														</a>
													</form>
												<?php	} else { ?>
													<form enctype="multipart/form-data" method="post" action="actionsadmin.php" style=" display: inline-block;">
														<a href="#">
															<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
															<input type="hidden" name="N2"  value="activer">
															<button  type="submit" style="background-color:inherit; border: none;">
																<span class="glyphicon glyphicon-ok activer" style="margin-left: 10px;color:green; font-size: 12px;">activer </span>
															</button>
														</a>
													</form>
												</div>
											</td>
										</tr>
									<?php	}
									$i++;
								}
								?>
				</tbody>
			</table>
			</div>
		</div>
		<script type="text/javascript">

			var form1 = document.getElementsByClassName('form11');
			var form2 = document.getElementsByClassName('form2');
			var form3 = document.getElementsByClassName('form3');
			var form4 = document.getElementsByClassName('form4');
			var edit = document.getElementsByClassName('editer');
			var supp = document.getElementsByClassName('Supprimer');
			var act = document.getElementsByClassName('activer');
			var act_inverse = document.getElementsByClassName('activer_inverse');
			var edit = document.getElementsByClassName('editer');

			edit.addEventListener('hover',affich);
			function affich(){
				edit.innerHTML.style.display='none';
			}
			console.log(edit );

			affich();
		</script>
		<script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
	</body>
</html>

