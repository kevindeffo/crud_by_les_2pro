<?php 
	session_start();
 ?>

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../css/style2.css">
		<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
		<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">		
	</head>
	<body class="corps ">
		<?php 
			include("header.php");

		?>
		<?php 
		 	if ((isset($_SESSION['USER']) AND $_SESSION['USER']['niveau']==1)) {
		 		header('location: headerr_account.php');
		 	}elseif ((isset($_SESSION['USER']) AND $_SESSION['USER']['niveau']==2)) {
		 		header('location: headerr_account.php');
		 	}else{
		?>
		<div class="container-fluid ">
			<div class="row" >	
				<div class=" formcon col-md-offset-3  col-md-6 col-sm-offset-3  col-sm-6 " >
					<h3>CONNECTEZ VOUS</h3>

					<p style="text-align: center; color: red;">	<?php 	
							if (isset($_SESSION['message'] ) ) {
								echo $_SESSION['message'];
							} ?> 

						<?php	if (isset($_SESSION['messagesup']) ) {
								echo $_SESSION['messagesup'];
							}
					 ?> </p>	
					<form  method="post" action="traitementconnexion.php" id="myform" >
						<div class="row">
							<div class=" col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-6 col-xs-offset-1 col-xs-10 form2">
								<div class="form-group mb">
									<span class="glyphicon glyphicon-envelope"></span>
									<label>email</label>
									<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
								</div>
								<div class="form-group mb">
									<span class="glyphicon glyphicon-lock"></span>
									<label>mot de passe</label>
									<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
								</div>
								
								<div class="form-group mt">
									<div class="col-md-offset-4 col-md-6 col-sm-offset-2 col-sm-8">
										<input  type="submit"  class="  btn btn-block btn-info btn-success " value="connexion" style="" >		
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
				
	</body>
</html>
<?php }
			  unset ($_SESSION['message'],$_SESSION['messagesup']);
			?>