<?php 
	session_start();
 ?>

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
		<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">		
	</head>
	<body class="corps ">
		<?php 
			if (isset($_SESSION['ADMIN'])) {
				header('location:admin.php');
			}else{
		 ?>

		<div class="container-fluid ">
		<!-- 	<div class="row">
				<div class="breadcrumb col-md-offset-1 col-md-4 " style="background-color: inherit;" ><a style="color:white;"  href="../index.php">acceuil</a> </div>
			</div> -->
			<div class="row" >	
				<div class=" formcon col-md-offset-3  col-md-6 col-md-offset-3   "  >
					<h3>CONNECTEZ VOUS (ADMIN)</h3>

					<p style="text-align: center; color: red;	">	<?php 	
							if (isset($_SESSION['message'])) {
								echo $_SESSION['message'];
							}
					 ?> </p>	
					<form  method="post" action="traitementconnexionadmin.php" id="myform" >
						<div class="row">
							<div class=" col-md-offset-3 col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">
								<div class="form-group mb">
									<span class="glyphicon glyphicon-envelope"></span>
									<label>email</label>
									<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
								</div>
								<div class="form-group mb">
									<span class="glyphicon glyphicon-lock"></span>
									<label>mot de passe</label>
									<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
								</div>
								<div class="form-group">
									<div class="col-md-offset-4 col-md-6">
										<input  type="submit"  class="  btn btn-block btn-info btn-success " value="connexion" style="background-color:rgb(50, 89, 208) !important;" >		
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php } ?>
    </body>
		<?php 	
			unset($_SESSION['message']);
		 ?>
</html>