

<?php 
session_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style2.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<!-- <link rel="stylesheet" href="../bootstrap_4/css/bootstrap.min.css"> -->
	
	
</head>

<body class="bgprofil">

	<div class="container-fluid" style="">
		<div class="row">
			<div class=" col-md-12 col-sm-12 col-xs-12">
				<nav class="navbar navbar-expand ">
					<div class="col-md-offset-8 col-md-4 col-sm-offset-5 col-sm-6 col-xs-offset-2 col-xs-8">
						<ul class="nav navbar-nav pull-right " style="padding-right: 50px;">
							<li class="navbar-btn mr  nav1"><span class="nom"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></li>
							<li class="nav-item dropdown">
				       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<?php echo "<img class='profil' src='../images/".$_SESSION['USER']['photo']."'>" ?>
				        	</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="mon_profil.php">Mon profil</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="Deconnexion.php">Deconnexion</a>

								</div>
							</li>
						</ul>
					</div>
				</nav>
			</div> 
		</div>

	<div class="container">
		<div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-7 col-xs-offset-1 col-xs-10" style="color:red; font-size:30px; text-align:center; margin-top: 80px;">
		<?php 
			if (isset($_SESSION['messageniv']) ) {
			    echo $_SESSION['messageniv'];
			} 
		?>
		</div>
	</div>
	</div>
	<script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
</body>
	
</html> 
