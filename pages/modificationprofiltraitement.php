<?php session_start(); ?>

<?php 
	$bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
	if (isset($_POST) AND isset($_SESSION['USER']) AND !empty($_POST['newnom']) AND !empty(['newprenom'])) {


		if(!empty($_POST['newpwd1']) AND !empty($_POST['newpwd2'])){
			if($_POST['newpwd1']==$_SESSION['USER']['pwd']){
				$insertpwd = $bdd->prepare("UPDATE utilisateur SET pwd = ? WHERE email = ?");
				$insertpwd->execute(array($_POST['newpwd2'], $_SESSION['USER']['email']));
				$insertprenom = $bdd->prepare("UPDATE utilisateur SET prenom = ? WHERE email = ?");
				$insertprenom->execute(array($_POST['newprenom'], $_SESSION['USER']['email']));
				$insertnom = $bdd->prepare("UPDATE utilisateur SET nom = ? WHERE email = ?");
				$insertnom->execute(array($_POST['newnom'], $_SESSION['USER']['email']));
				$_SESSION['USER']['pwd']=$_POST['newpwd2'];
				$_SESSION['USER']['nom']=$_POST['newnom'];
				$_SESSION['USER']['prenom']=$_POST['newprenom'];
			    header('location:mon_profil.php');
				// $_SESSION['messagepwd1']='modification de mot de passe reussi';
				
		    }else{
				 $_SESSION['messagepwd']='ancien mot de passe incorrect';
				 header('location:mon_profil.php');
		    	    }
	    }else {
			$insertprenom = $bdd->prepare("UPDATE utilisateur SET prenom = ? WHERE email = ?");
			$insertprenom->execute(array($_POST['newprenom'], $_SESSION['USER']['email']));
			$insertnom = $bdd->prepare("UPDATE utilisateur SET nom = ? WHERE email = ?");
			$insertnom->execute(array($_POST['newnom'], $_SESSION['USER']['email']));
			$_SESSION['USER']['nom']=$_POST['newnom'];
			$_SESSION['USER']['prenom']=$_POST['newprenom'];
			$_SESSION['messagesuc']='Modifications Reussie';
			header('location:mon_profil.php');
	    }

            if (isset($_FILES['newphoto']) AND $_FILES['newphoto']['error'] == 0){
                if($_FILES['newphoto']['name']!=$_SESSION['USER']['photo']){
                    if($_FILES['newphoto']['size'] <= 3000000){ 
                        // on insere la valeur dans la bdd
                        $insertphoto = $bdd->prepare("UPDATE utilisateur SET photo = ? WHERE email = ?");
                        $insertphoto->execute(array($_FILES['newphoto']['name'], $_SESSION['USER']['email']));
                        move_uploaded_file($_FILES['newphoto']['tmp_name'], '../images/' . basename($_FILES['newphoto']['name']));
                        $_SESSION['USER']['photo']=$_FILES['newphoto']['name'];

                    }
                    
                }
            }

	}

 ?>