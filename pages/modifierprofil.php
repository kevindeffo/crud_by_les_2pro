<?php 
	session_start();
 ?>



 <!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
		<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">		
	</head>
	<body>
		<div class="col-md-offset-1 col-md-5">
			<form action="modificationprofiltraitement.php" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="">Nom</label>
				<input type="text" name="newnom" class="form-control" value="<?php if(isset($_SESSION['USER'])){ echo $_SESSION['USER']['nom'];} ?>">
				</div>
				<div class="form-group">
					<label for="">Prenom</label>
				<input type="text" name="newprenom" class="form-control" value="<?php if(isset($_SESSION['USER'])){ echo $_SESSION['USER']['prenom'];} ?>">
				</div>
<!-- 				<dv class="form-group">
					<label for="">Prenom</label>
				<input type="email" name="newemail" class="form-control" value="<?php if(isset($_SESSION['USER'])){ echo $_SESSION['USER']['email'];} ?>">
				</dv> -->
				<div class="form-group">
					<label for="">Ancien Mot de passe</label>
				<input type="password" name="newpwd1" class="form-control">
				</div>
				<div class="form-group">
					<label for="">Nouveau Mot de passe</label>
				<input type="password" name="newpwd2" class="form-control" >
				</div>
				<div class="col-md-offset-1 col-md-7 col-md-pull-1 col-xm-5 col-xs-offset-1 col-xs-11 "> 
					<span class="glyphicon glyphicon-picture"></span>
					<label>Telecharger votre nouvelle photo de pofil</label>
					<input class="form-control inpt7" type="file" name="newphoto" accept="image/*" onchange="loadFile(event)" required="" value="<?php echo $_SESSION['USER']['photo'];?>" >
					<div class="col-md-offset-4 col-md-4 im" style="">
						<?php if(isset($_SESSION['USER'])){ echo "<img id='im' src='../images/".$_SESSION['USER']['photo']."'/>";} ?> 
					</div>
				</div>
				<div class="form-group col-md-offset-3 col-md-7">
					<input type="submit" class="form-control" value="Sauvegarder les modifications">
				</div> 
			</form>	
		</div>
		<script type="text/javascript">
			var loadFile = function(event) {
				var profil = document.getElementById('im');
				profil.src = URL.createObjectURL(event.target.files[0]);
			};
		</script>
	</body>
</html>