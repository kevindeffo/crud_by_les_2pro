<?php session_start(); ?>

<!DOCTYPE html>
<html>

<head>
    <title>COMPTE INSCRIPT</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="../css/style2.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
    <meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">


</head>

<body class="bgprofil ">
    <div class="container">
        <nav class="navbar">
            <div class="navbar-header ">
                <ul class="nav navbar-nav">
                    <li style="margin-right: 50px;"><a style="color:#fff;" href="headerr_account.php"> <span class="fa fa-arrow-left"> Retour</span></a></li>
                </ul>
            </div>
        </nav>

        <div class="container col-md-offset-3 col-md-6 col-sm-offset-2 col-md-8 col-xs-offset-1 col-xs-10 profil2"  style="background-color:rgba(0, 0, 0, 0.7); color: #fff; border-radius: 15px; ">
            <div class="row">
                <div class="col-md-offset-4 col-md-5 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-xs-10">
                    <h3>MON PROFIL</h3>
                    <p></p>
                </div>
            </div>
            <p style="text-align: center; color: green;"> 
            <?php if (isset($_SESSION['messagesuc'])) { ?><span class="fa fa-check"></span>
              <?php  echo $_SESSION['messagesuc'];
            } ?>
            </p>
            <p style="text-align: center; color: red;">
             <?php if (isset($_SESSION['messagepwd'])) { ?> <span class="fa fa-times"></span>
               <?php  echo $_SESSION['messagepwd'];
            } ?>
            </p>
            <div class="row">
                <div class="col-md-offset-3 col-md-2 col-sm-offset-2 col-sm-3 col-xs-offset-2 col-xs-6">
                    <?php echo '<img src="../images/'.$_SESSION['USER']['photo'].' " style="height:300px; width:300px;  margin-bottom:30px;" >'?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-7 col-xs-offset-1 col-xs-9">
                    <p style="font-weight: bold;">
                    <?php echo 'Nom:'?> &nbsp &nbsp &nbsp &nbsp &nbsp <?php echo $_SESSION['USER']['nom'];?> <br>
                    </p>
                    <p style="font-weight: bold;">
                        <?php   echo 'Prenom:'?> &nbsp   &nbsp &nbsp &nbsp &nbsp<?php echo $_SESSION['USER']['prenom']; ?>
                    </p>
                    <p style="font-weight: bold;">
                        <?php 
                            echo 'Email:'?> <?php echo $_SESSION['USER']['email'];
                         ?>
                    </p>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-6">
                    <a class="btn btn-success mb" href="#" data-toggle="modal" data-target="#myModal" style="<?php if ( $_SESSION['USER']['niveau']==2) { echo 'display: none';} ?>">Modifier Mon profil</a>
                </div>
            </div>
        </div>



        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="form1_2">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">Modification de Profil</h2>
                    <div class="">

                        <form enctype="multipart/form-data" method="post" action="modificationprofiltraitement.php" id="myform">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="">

                                        <span class="glyphicon glyphicon-picture"></span>
                                        <label>Telecharger votre photo de pofil<?php echo "src='../images/" . $_SESSION['USER']['photo'] . "'" ?></label>
                                        <input class="form-control inpt7" type="file" name="newphoto" accept="image/*" onchange="loadFile(event)" value="<?php echo $_SESSION['USER']['photo'] ?>">
                                        <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-md-5 im">
                                            <img id="im" <?php echo "src='../images/" . $_SESSION['USER']['photo'] . "'" ?> />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="">
                                        <span class="glyphicon glyphicon-user"></span>
                                        <label>Noms</label>
                                        <input id="name" class="form-control inpt1 " type="text" name="newnom" value="<?php if (isset($_SESSION['USER'])) {
                                                                                                                        echo $_SESSION['USER']['nom'];
                                                                                                                    } ?>" required="">
                                        <p></p>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <label>Prenoms</label>
                                        <input class="form-control inpt2" type="text" name="newprenom" id="prenom" value="<?php if (isset($_SESSION['USER'])) {
                                                                                                                            echo $_SESSION['USER']['prenom'];
                                                                                                                        } ?>" required="">
    
                                        <p></p>
                                        <span class="glyphicon glyphicon-lock"></span>
                                        <label>Ancien mot de passe</label>
                                        <input class="form-control inpt4" type="password" name="newpwd1" id="pwd" placeholder="Entrez l'ancien mot de passe"><br>
                                        <input type="hidden" name="niveau" value="1">

                                        <p></p>
                                        <span class="glyphicon glyphicon-lock"></span>
                                        <label>Nouveau mot de passe</label>
                                        <input class="form-control inpt4" type="password" name="newpwd2" id="pwd" placeholder="Entrez le nouveau mot de passe"><br>
                                        <input type="hidden" name="niveau" value="1">
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-offset-4 col-md-4 col-xm-5 col-xs-6">
                                    <input type="submit" value="Sauvegarder" class="btn btn-block btn-info btn-success">
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var loadFile = function(event) {
            var profil = document.getElementById('im');
            profil.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>


    <script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
<?php 
    unset($_SESSION['messagesuc'],$_SESSION['messagepwd']);
 ?>