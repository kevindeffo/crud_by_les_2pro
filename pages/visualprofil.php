<?php session_start(); ?>

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../css/style2.css">
		<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
		<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">		
	</head>
	<body class=" bgprofil">
		    <div class="row">
			    <div class=" col-md-12">
				   <nav class="navbar navbar-expand ">
				   		<div class="col-md-offset-1 col-md-1" style="color:#fff; margin-top: 40px;" ><a href="admin.php" style="color:#fff;"> <span class="fa fa-arrow-left"> Retour</span></a></div>
					   <div class="col-md-offset-6 col-md-4">
						    <ul class="nav navbar-nav pull-right " style="padding-right: 50px;">
								<li class="navbar-btn mr  nav1"><span class="nom"> <?php echo $_SESSION['ADMIN']['nom']." ".$_SESSION['ADMIN']['prenom'] ?></li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<?php echo "<img class='profil ' src='../images/".$_SESSION['ADMIN']['photo']."'>" ?>
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="Deconnexionadmin.php">Deconnexion</a>
										<div class="dropdown-divider"></div>
									</div>
								</li>
							</ul>
						</div>
					</nav>
				</div> 
			</div>
<!-- 			<a style="color:#fff; margin-left: 40px;" href="admin.php"> <span class="fa fa-arrow-left"> Retour</span></a> -->
	    <div class="container">
	        <div class="container col-md-offset-3 col-md-6 profil2"  style="background-color:rgba(0, 0, 0, 0.7); color: #fff; border-radius: 15px; ">

	            <div class="row">
	                <div class="col-md-offset-4 col-md-5" >
	                    <h3>PROFIL  UTILISATEUR</h3>
	                    <p></p>
	                </div>
	            </div>
	         	<p style="color:green; text-align: center;"><?php 
	        		if(isset($_SESSION['succes'])) { ?>  <span class="fa fa-check"></span>
	        		<?php	echo $_SESSION['succes'];
	        		} ?> 
	        	</p>
	            <div class="row">
	                <div class="col-md-offset-3 col-md-2 mb">
	                    <?php echo '<img classe="thumbnail" src="../images/'.$_SESSION['USERM']['photo'].' " style="height:300px; width:300px; " >'?>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-offset-3 col-md-6 ">
	                    <p style="font-weight: bold;">
	                    <?php echo 'Nom:'?> &nbsp &nbsp &nbsp &nbsp &nbsp <?php echo $_SESSION['USERM']['nom'];?> <br>
	                    </p>
	                    <p style="font-weight: bold;">
	                        <?php   echo 'Prenom:'?> &nbsp   &nbsp &nbsp &nbsp &nbsp<?php echo $_SESSION['USERM']['prenom']; ?>
	                    </p>
	                    <p style="font-weight: bold;">
	                        <?php 
	                            echo 'Email:'?> <?php echo $_SESSION['USERM']['email'];
	                         ?>
	                    </p>
	                </div>  
	            </div>
	            <div class="row">
	                <div class="col-md-offset-4 col-md-4">
	                    <a class="btn btn-success mb" href="#" data-toggle="modal" data-target="#myModal">Modifier Mon profil</a>
	                </div>
	            </div>
	        </div>



	        <div class="modal fade" id="myModal">
	            <div class="modal-dialog">
	                <div class="form1_2">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <h2 class="modal-title">Modification de Profil</h2>
	                    <div class="">

	                        <form enctype="multipart/form-data" method="post" action="modificationprofiltraitement2.php" id="myform">
	                            <div class="row">
	                                <div class="col-md-12">
	                                	 
	                                    <div class="">

	                                        <span class="glyphicon glyphicon-picture"></span>
	                                        <label>Telecharger votre photo de pofil<?php echo "src='../images/" . $_SESSION['USERM']['photo'] . "'" ?></label>
	                                        <input class="form-control inpt7" type="file" name="newphoto" accept="image/*" onchange="loadFile(event)" value="<?php echo $_SESSION['USERM']['photo'] ?>">
	                                        <div class="col-md-offset-4 col-md-4 im">
	                                            <img id="im" <?php echo "src='../images/" . $_SESSION['USERM']['photo'] . "'" ?> />
	                                        </div>
	                                    </div>
	                                </div>

	                                <div class="col-md-12">
	                                    <div class="">
	                                        <span class="glyphicon glyphicon-user"></span>
	                                        <label>Noms</label>
	                                        <input id="name" class="form-control inpt1 " type="text" name="newnom" value="<?php if (isset($_SESSION['USERM'])) {
	                                                                                                                        echo $_SESSION['USERM']['nom'];
	                                                                                                                    } ?>" required="">
	                                        <p></p>
	                                        <span class="glyphicon glyphicon-user"></span>
	                                        <label>Prenoms</label>
	                                        <input class="form-control inpt2" type="text" name="newprenom" id="prenom" value="<?php if (isset($_SESSION['USERM'])) {
	                                                                                                                            echo $_SESSION['USERM']['prenom'];
	                                                                                                                        } ?>" required="">
	    
	                                        <p></p>
	                                        <span class="glyphicon glyphicon-lock"></span>
	                                        <label>Ancien mot de passe</label>
	                                        <input class="form-control inpt4" type="password" name="newpwd1" id="pwd" placeholder="Entrez l'ancien mot de passe"><br>
	                                        <input type="hidden" name="niveau" value="1">

	                                        <p></p>
	                                        <span class="glyphicon glyphicon-lock"></span>
	                                        <label>Nouveau mot de passe</label>
	                                        <input class="form-control inpt4" type="password" name="newpwd2" id="pwd" placeholder="Entrez le nouveau mot de passe"><br>
	                                        <input type="hidden" name="niveau" value="1">
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-md-offset-4 col-md-4 col-xm-5 col-xs-6">
	                                <input type="submit" value="modifier" class="btn btn-block btn-info btn-success">
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>

	    <script type="text/javascript">
	        var loadFile = function(event) {
	            var profil = document.getElementById('im');
	            profil.src = URL.createObjectURL(event.target.files[0]);
	        };
	    </script>


	    <script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
	    <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>