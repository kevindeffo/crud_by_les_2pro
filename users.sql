-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/nju
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 02 juil. 2021 à 15:52
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `users`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `photo`, `email`, `pwd`, `niveau`) VALUES
(1, 'Super', 'admin', 'team2.jpg', 'superadmin@gmail.com', 'superadmin', 5),
(3, 'KAMGA', 'FRANK', 'images (12).jpg', 'deffokevin14@gmail.com', 'scroll', 3),
(4, 'bagna', 'fiol', '2720_854x480.jpg', 'deffokevin15@gmail.com', 'fffffffff', 2),
(16, 'DEFFO', 'KEVIN', 'math4.jpg', 'deffokevin17@gmail.com', 'dsdsfsfsfsfsfsfsfsfsf', 3),
(18, 'BAMBA', 'RICHARDO', 'images (10).jpg', 'deffokevin16@gmail.com', 'vvvvvvvvv', 2),
(22, 'DEFFO', 'KEVIN', '2720_854x480.jpg', 'deffokeviny@gmail.com', 'hghghghghghghgh', 1),
(24, 'NANFo', 'LEONEL', 'canstockphoto17976798.jpg', 'deffokevi@gmail.com', 'frfrfrfr', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
